Name:           gstreamer1-libav
Version:        1.24.11
Release:        1
Summary:        GStreamer plugin, a set of codecs plugins based on the ffmpeg library.
License:        LGPL-2.1-or-later
URL:            https://gstreamer.freedesktop.org/
Source0:        https://gstreamer.freedesktop.org/src/gst-libav/gst-libav-%{version}.tar.xz

BuildRequires: gcc
BuildRequires: meson >= 1.1
BuildRequires: pkgconfig(gstreamer-1.0) >= %{version}
BuildRequires: pkgconfig(gstreamer-audio-1.0) >= %{version}
BuildRequires: pkgconfig(gstreamer-base-1.0) >= %{version}
BuildRequires: pkgconfig(gstreamer-check-1.0) >= %{version}
BuildRequires: pkgconfig(gstreamer-pbutils-1.0) >= %{version}
BuildRequires: pkgconfig(gstreamer-video-1.0) >= %{version}
BuildRequires: pkgconfig(libavcodec) >= 58.18.100
BuildRequires: pkgconfig(libavfilter) >= 7.16.100
BuildRequires: pkgconfig(libavformat) >= 58.12.100
BuildRequires: pkgconfig(libavutil) >= 56.14.100
Requires: gstreamer1 >= %{version} gstreamer1-plugins-base >= %{version}

%description
This module contains a GStreamer plugin for using the encoders, decoders, muxers,
and demuxers provided by FFmpeg. It is called gst-libav for historical reasons.

a set of codecs plugins based on the ffmpeg library. This is where you can find audio
and video decoders and encoders for a wide variety of formats including H.264, AAC, etc.

%prep
%autosetup -p1 -n gst-libav-%{version}

%build
%meson -D doc=disabled
%meson_build

%install
%meson_install

%files
%doc AUTHORS ChangeLog NEWS README.md
%license COPYING
%{_libdir}/gstreamer-1.0/libgstlibav.so

%changelog
* Wed Jan 29 2025 Funda Wang <fundawang@yeah.net> - 1.24.11-1
- update to 1.24.11

* Wed Jun 26 2024 liweigang <liweiganga@uniontech.com> - 1.24.0-1
- update to version 1.24.0

* Wed Nov 22 2023 lwg <liweiganga@uniontech.com> - 1.22.5-1
- update to version 1.22.5

* Mon Jun 20 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1.18.4-2
- add gstreamer1-libav.yaml

* Wed Sep 29 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 1.18.4-1
- Package gstreamer1-libav init with 1.18.4
